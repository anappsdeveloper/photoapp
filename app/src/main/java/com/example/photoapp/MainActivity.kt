package com.example.photoapp

import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.OutlinedTextField
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import kotlin.math.sqrt

import androidx.compose.material.Surface
import androidx.compose.material3.Button
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.text.input.KeyboardType
import coil.compose.rememberAsyncImagePainter
import com.example.photoapp.ui.theme.MainViewModel


class MainActivity : ComponentActivity() {

    private val viewModel: MainViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApp(viewModel)
        }

        viewModel.pickImages(this)
    }
}

@Composable
fun MyApp(viewModel: MainViewModel) {

    Surface(color = MaterialTheme.colorScheme.background) {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {


            Spacer(modifier = Modifier.height(16.dp))

            if (viewModel.selectedImages.isNotEmpty()) {
                ImageList(viewModel.selectedImages)
            }
        }
    }
}

@Composable
fun ImageList(selectedImages: List<Uri>) {
    var listSize by remember { mutableStateOf(50) }
   /* val generatedList = remember(selectedImages) {*/
     val generatedList =  generateList(selectedImages,listSize)
   /* }*/

    Column ( modifier = Modifier
        .fillMaxSize()
        .padding(16.dp)){
            Text(
                text = "Select Images and Specify List Size",
                style = MaterialTheme.typography.titleSmall,
                modifier = Modifier.padding(bottom = 16.dp)
            )

    OutlinedTextField(
        value = listSize.toString(),
        onValueChange = {
            listSize = it.toIntOrNull() ?: 0
        },
        label = { Text("Enter List Size") },
        keyboardOptions = KeyboardOptions.Default.copy(
            keyboardType = KeyboardType.Number
        ),
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 16.dp)
    )
        LazyColumn {
            itemsIndexed(generatedList) { index, pair ->
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier.padding(8.dp)
                ) {
                    Image(
                        painter = rememberAsyncImagePainter(pair.first),
                        contentDescription = null,
                        modifier = Modifier.size(200.dp)
                    )

                    Spacer(modifier = Modifier.width(16.dp))

                    Text(
                        text = pair.second.toString(),
                        style = MaterialTheme.typography.titleSmall
                    )
                }
            }
        }
    }
}

@Composable
fun generateList(selectedImages: List<Uri>, listSize: Int): List<Pair<Uri, Int>> {
    val generatedList = mutableListOf<Pair<Uri, Int>>()
    val numImages = selectedImages.size
    val numRows = sqrt(numImages.toDouble() * listSize).toInt()
    var currentIndex = 0
    for (row in 1..numRows) {
        for (col in 1..row) {
            if (generatedList.size < 50) {
                generatedList.add(selectedImages[currentIndex] to generatedList.size + 1)
                currentIndex = (currentIndex + 1) % numImages
            } else {
                return generatedList
            }
        }
    }
    return generatedList
}