package com.example.photoapp.ui.theme

import android.net.Uri
import androidx.activity.ComponentActivity
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch


class MainViewModel : ViewModel() {
val _selectedImages = mutableStateOf<List<Uri>>(emptyList())
    val selectedImages: List<Uri> by _selectedImages
    fun pickImages(activity: ComponentActivity) {

        val imagePickerLauncher = activity.registerForActivityResult(ActivityResultContracts.GetMultipleContents()) { uris ->
            viewModelScope.launch {
                _selectedImages.value = uris
            }
        }
        imagePickerLauncher.launch("image/*")
    }
    }


